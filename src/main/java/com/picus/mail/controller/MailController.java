package com.picus.mail.controller;

import com.picus.mail.data.CampaignDto;
import com.picus.mail.data.ContactDto;
import com.picus.mail.data.MailDto;
import com.picus.mail.data.MailReportDto;
import com.picus.mail.link.LinkGenerator;
import com.picus.mail.mail.MailSenderTask;
import com.picus.mail.service.MailReportService;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

@RequestMapping("/mail")
@CrossOrigin
@RestController
public class MailController {

    private final static Logger LOGGER = Logger.getLogger(MailController.class);

    @Autowired
    private MailReportService mailReportService;

    @Autowired
    private LinkGenerator linkGenerator;

    @Autowired
    private ThreadPoolExecutor executor;

    @Autowired
    private ApplicationContext ctx;

    @Value("${mail.link}")
    private String mailLink;

    @PostMapping(value = "/send", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void send(@RequestBody CampaignDto campaign) {
        if(campaign != null && !campaign.getContactList().isEmpty()) {
            for (ContactDto contact : campaign.getContactList()) {
                String mailAddress = contact.getMailAddress();
                String hash = linkGenerator.generateHash(mailAddress);
                String url = mailLink + hash;
                String mailBody = campaign.getBody() + "\r\n" + url;
                MailDto mailDto = new MailDto(mailAddress, campaign.getName(), mailBody, hash);
                MailSenderTask task = ctx.getBean(MailSenderTask.class);
                task.setMail(mailDto);
                executor.execute(task);
            }
        }
    }

    @PostMapping(value = "/acknowledge")
    public void acknowledge(@RequestBody String userKey) {
        long clickedTime = System.currentTimeMillis();
        MailReportDto dto = mailReportService.findByKey(userKey);
        if(dto != null) {
            dto.setClickedTime(clickedTime);
            dto.setTimeDiff(calculateTimeDiff(dto.getClickedTime() - dto.getSentTime()));
            mailReportService.save(dto);
        } else {
            LOGGER.error("Record is not found or updated before with the provided key : " + userKey);
        }
    }

    @GetMapping(value = "/getAllReports")
    public ResponseEntity<List<MailReportDto>> getAllReports() {
        List<MailReportDto> reports = mailReportService.findAll();
        return ResponseEntity.ok(reports);
    }

    private String calculateTimeDiff(long diff) {
        String day = DurationFormatUtils.formatDuration(diff, "d");
        String hour = DurationFormatUtils.formatDuration(diff, "HH:mm:ss");
        return day + " day " + hour;
    }
}
