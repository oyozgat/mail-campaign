package com.picus.mail.controller;

import com.picus.mail.data.ContactDto;
import com.picus.mail.data.ParserReportDto;
import com.picus.mail.parser.ContactFileParser;
import com.picus.mail.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@CrossOrigin
@RequestMapping("/file")
public class FileController {

    private static final String UPLOADED_FOLDER = "C:\\Users\\prog\\Desktop\\picus\\uploaded\\";

    @Value("${upload.url}")
    private String uploadUrl;

    @Autowired
    private ContactFileParser fileParser;

    @Autowired
    private ContactService contactService;

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ParserReportDto> upload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        ParserReportDto report;
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Path uploadedPath = Files.write(path, bytes);

            report = fileParser.parse(uploadedPath.toUri().getPath());
            for (ContactDto contactDto : report.getContacts()) {
                contactService.save(contactDto);
            }
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(report);
    }

}
