package com.picus.mail.controller;

import com.picus.mail.data.ContactDto;
import com.picus.mail.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/contact")
@CrossOrigin
@RestController
public class ContactController {

    @Autowired
    private ContactService contactService;

    @PostMapping("/save")
    public ContactDto save(@RequestBody ContactDto contactDto) {
        return contactService.save(contactDto);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody ContactDto contactDto) {
        contactService.delete(contactDto);
    }

    @GetMapping("/getAll")
    public List<ContactDto> getAllContacts() {
        return contactService.findAll();
    }

}
