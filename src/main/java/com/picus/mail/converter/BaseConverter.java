package com.picus.mail.converter;

import com.picus.mail.data.BaseDto;
import com.picus.mail.entity.BaseEntity;

public interface BaseConverter <T extends BaseDto, E extends BaseEntity>{

    T convertToDto(E entity);

    E convertToEntity(T dto);

}
