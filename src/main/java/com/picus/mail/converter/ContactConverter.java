package com.picus.mail.converter;

import com.picus.mail.data.ContactDto;
import com.picus.mail.entity.ContactEntity;
import org.springframework.stereotype.Component;

@Component
public class ContactConverter implements BaseConverter<ContactDto, ContactEntity> {

    @Override
    public ContactDto convertToDto(ContactEntity entity) {
        return new ContactDto(entity.getId(), entity.getFullName(), entity.getMailAdress());
    }

    @Override
    public ContactEntity convertToEntity(ContactDto dto) {
        return new ContactEntity(dto.getFullName(), dto.getMailAddress());
    }

}
