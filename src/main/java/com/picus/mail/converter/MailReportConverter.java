package com.picus.mail.converter;

import com.picus.mail.data.MailReportDto;
import com.picus.mail.entity.MailReportEntity;
import org.springframework.stereotype.Component;

@Component
public class MailReportConverter implements BaseConverter<MailReportDto, MailReportEntity> {

    @Override
    public MailReportDto convertToDto(MailReportEntity entity) {
        return new MailReportDto(entity.getId(), entity.getMailAddress(), entity.getKey(), entity.getSentTime(), entity.getClickedTime(), entity.getTimeDiff());
    }

    @Override
    public MailReportEntity convertToEntity(MailReportDto dto) {
        return new MailReportEntity(dto.getId(), dto.getMailAddress(), dto.getKey(), dto.getSentTime(), dto.getClickedTime(), dto.getTimeDiff());
    }
}
