package com.picus.mail.mail;

import com.picus.mail.data.MailDto;

public interface MailSender {

    void send(MailDto mailDto);

}
