package com.picus.mail.mail;

import com.picus.mail.data.MailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class MailSenderTask implements Runnable {

    @Autowired
    MailSender mailSender;

    private MailDto mail;

    public MailSenderTask() {
    }

    public void setMail(MailDto mail) {
        this.mail = mail;
    }

    @Override
    public void run() {
        mailSender.send(mail);
    }
}
