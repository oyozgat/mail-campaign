package com.picus.mail.mail;

import com.picus.mail.data.MailDto;
import com.picus.mail.data.MailReportDto;
import com.picus.mail.link.LinkGenerator;
import com.picus.mail.service.MailReportService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class MailSenderImpl implements MailSender {

    private final static Logger LOGGER = Logger.getLogger(MailSenderImpl.class);


    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MailReportService mailReportService;

    @Autowired
    private LinkGenerator linkGenerator;

    @Override
    @Transactional
    public void send(MailDto mailDto) {
        try {
            SimpleMailMessage msg = new SimpleMailMessage();

            msg.setTo(mailDto.getTo());
            msg.setSubject(mailDto.getSubject());
            msg.setText(mailDto.getBody());
            mailSender.send(msg);
            saveMailReport(mailDto);

            LOGGER.info("Mail is sent to :" + mailDto.getTo());
        } catch (Exception e) {
            LOGGER.error("An error occured while sending mailDto : " + e);
        }
    }

    private void saveMailReport(MailDto mailDto) {
        MailReportDto mailReportDto = new MailReportDto();
        mailReportDto.setMailAddress(mailDto.getTo());
        mailReportDto.setKey(mailDto.getGeneratedKey());
        mailReportDto.setSentTime(System.currentTimeMillis());
        mailReportDto.setClickedTime(0L);
        mailReportDto.setTimeDiff("Not clicked yet");
        mailReportService.save(mailReportDto);
    }
}
