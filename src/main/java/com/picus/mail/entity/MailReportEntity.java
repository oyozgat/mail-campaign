package com.picus.mail.entity;

import javax.persistence.*;

@Entity(name = "MAIL_REPORT")
public class MailReportEntity implements BaseEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Column(name = "MAIL_ADDRESS")
    private String mailAddress;

    @Column(name = "GENERATED_KEY")
    private String key;

    @Column(name = "SENT_TIME")
    private long sentTime;

    @Column(name = "CLICKED_TIME")
    private long clickedTime;

    @Column(name = "DIFF_TIME")
    private String timeDiff;

    public MailReportEntity() {
        //default constructor
    }

    public MailReportEntity(int id, String mailAddress, String key, long sentTime, long clickedTime, String timeDiff) {
        this.id = id;
        this.mailAddress = mailAddress;
        this.key = key;
        this.sentTime = sentTime;
        this.clickedTime = clickedTime;
        this.timeDiff = timeDiff;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getSentTime() {
        return sentTime;
    }

    public void setSentTime(long sentTime) {
        this.sentTime = sentTime;
    }

    public long getClickedTime() {
        return clickedTime;
    }

    public void setClickedTime(long clickedTime) {
        this.clickedTime = clickedTime;
    }

    public String getTimeDiff() {
        return timeDiff;
    }

    public void setTimeDiff(String timeDiff) {
        this.timeDiff = timeDiff;
    }
}
