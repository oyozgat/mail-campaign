package com.picus.mail.entity;

import javax.persistence.*;

@Entity(name = "CONTACT")
public class ContactEntity implements BaseEntity{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "MAIL_ADDRESS")
    private String mailAdress;

    public ContactEntity() {
        // default constructor
    }

    public ContactEntity(String fullName, String mailAdress) {
        this.fullName = fullName;
        this.mailAdress = mailAdress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMailAdress() {
        return mailAdress;
    }

    public void setMailAdress(String mailAdress) {
        this.mailAdress = mailAdress;
    }
}
