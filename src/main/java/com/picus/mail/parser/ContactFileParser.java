package com.picus.mail.parser;

import com.picus.mail.data.ParserReportDto;

public interface ContactFileParser {

    ParserReportDto parse(String filePath);

}
