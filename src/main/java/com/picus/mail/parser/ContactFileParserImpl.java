package com.picus.mail.parser;

import com.picus.mail.data.ContactDto;
import com.picus.mail.data.ParserReportDto;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class ContactFileParserImpl implements ContactFileParser {

    private final static Logger LOGGER = Logger.getLogger(ContactFileParserImpl.class);

    @Value("${contact.parser.regex}")
    private String regex;

    @Override
    public ParserReportDto parse(String filePath) {
        ParserReportDto report = new ParserReportDto();
        List<ContactDto> contactDtos = new ArrayList<>();
        int successCount = 0;
        int failedCount = 0;
        try {
            FileReader fileReader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != null) {
                try {
                    String[] contactElements = currentLine.trim().split(regex);
                    contactDtos.add(new ContactDto(contactElements[0], contactElements[1]));
                    successCount++;
                } catch (Exception e) {
                    LOGGER.error("Provided format is not valid : " + currentLine);
                    failedCount++;
                }
            }
        } catch (Exception ex) {
            LOGGER.error("An error occurred while reading file:" + ex);
        }
        report.setContacts(contactDtos);
        report.setSuccessCount(successCount);
        report.setFailCount(failedCount);
        return report;
    }
}
