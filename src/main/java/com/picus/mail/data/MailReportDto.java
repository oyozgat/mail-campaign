package com.picus.mail.data;

public class MailReportDto implements BaseDto {

    private int id;

    private String mailAddress;

    private String key;

    private long sentTime;

    private long clickedTime;

    private String timeDiff;

    public MailReportDto() {
        //default constructor
    }

    public MailReportDto(int id, String mailAddress, String key, long sentTime, long clickedTime, String timeDiff) {
        this.id = id;
        this.mailAddress = mailAddress;
        this.key = key;
        this.sentTime = sentTime;
        this.clickedTime = clickedTime;
        this.timeDiff = timeDiff;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getSentTime() {
        return sentTime;
    }

    public void setSentTime(long sentTime) {
        this.sentTime = sentTime;
    }

    public long getClickedTime() {
        return clickedTime;
    }

    public void setClickedTime(long clickedTime) {
        this.clickedTime = clickedTime;
    }

    public String getTimeDiff() {
        return timeDiff;
    }

    public void setTimeDiff(String timeDiff) {
        this.timeDiff = timeDiff;
    }
}
