package com.picus.mail.data;

import java.io.Serializable;

public class MailDto implements Serializable {

    private String to;
    private String subject;
    private String body;
    private String generatedKey;

    public MailDto() {
        //default constructor
    }

    public MailDto(String to, String subject, String body, String generatedKey) {
        this.to = to;
        this.subject = subject;
        this.body = body;
        this.generatedKey = generatedKey;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getGeneratedKey() {
        return generatedKey;
    }

    public void setGeneratedKey(String generatedKey) {
        this.generatedKey = generatedKey;
    }
}
