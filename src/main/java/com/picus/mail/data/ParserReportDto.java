package com.picus.mail.data;

import java.util.List;

public class ParserReportDto {

    private List<ContactDto> contacts;
    private int successCount;
    private int failCount;

    public ParserReportDto() {
        //default constructor
    }

    public ParserReportDto(List<ContactDto> contacts, int successCount, int failCount) {
        this.contacts = contacts;
        this.successCount = successCount;
        this.failCount = failCount;
    }

    public List<ContactDto> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactDto> contacts) {
        this.contacts = contacts;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    public int getFailCount() {
        return failCount;
    }

    public void setFailCount(int failCount) {
        this.failCount = failCount;
    }
}
