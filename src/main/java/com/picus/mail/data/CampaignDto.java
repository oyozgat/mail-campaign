package com.picus.mail.data;

import java.io.Serializable;
import java.util.List;

public class CampaignDto implements Serializable {

    private String name;

    private String body;

    private List<ContactDto> contactList;

    public CampaignDto() {
        //default constructor
    }

    public CampaignDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<ContactDto> getContactList() {
        return contactList;
    }

    public void setContactList(List<ContactDto> contactList) {
        this.contactList = contactList;
    }
}
