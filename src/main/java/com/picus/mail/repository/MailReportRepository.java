package com.picus.mail.repository;

import com.picus.mail.entity.MailReportEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailReportRepository extends CrudRepository<MailReportEntity, Integer> {

    MailReportEntity findOneByKeyAndClickedTime(String key, long clickedTime);

}
