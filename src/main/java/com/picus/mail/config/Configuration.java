package com.picus.mail.config;

import org.springframework.context.annotation.Bean;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public ThreadPoolExecutor taskExecutor() {
        return (ThreadPoolExecutor) Executors.newCachedThreadPool();
    }

}
