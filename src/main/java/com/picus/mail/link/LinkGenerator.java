package com.picus.mail.link;

public interface LinkGenerator {
    String generateHash(String text);
}
