package com.picus.mail.link;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class LinkGeneratorImpl implements LinkGenerator {

    private final static Logger LOGGER = Logger.getLogger(LinkGeneratorImpl.class);

    @Value("${hash.algorithm}")
    private String algorithm;

    @Override
    public String generateHash(String text) {
        MessageDigest md = null;
        text = text + System.currentTimeMillis();
        try {
            md = MessageDigest.getInstance(algorithm);
            md.update(text.getBytes());
            byte[] digest = md.digest();
            BigInteger no = new BigInteger(1, digest);
            String hashText = no.toString(16);
            while (hashText.length() < 32) {
                hashText = "0" + hashText;
            }
            return hashText;
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Provided algorithm is not supported : " + algorithm);
        }

        return "";
    }

}
