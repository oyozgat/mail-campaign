package com.picus.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 */
@SpringBootApplication
public class MailCampaignApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailCampaignApplication.class, args);
    }

}
