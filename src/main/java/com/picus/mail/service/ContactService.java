package com.picus.mail.service;

import com.picus.mail.data.ContactDto;

import java.util.List;

public interface ContactService {

    ContactDto save(ContactDto dto);

    void delete(ContactDto dto);

    List<ContactDto> findAll();

    ContactDto findById(int id);
}
