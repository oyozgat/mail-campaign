package com.picus.mail.service;

import com.picus.mail.data.MailReportDto;

import java.util.List;

public interface MailReportService {

    MailReportDto save(MailReportDto dto);

    List<MailReportDto> findAll();

    MailReportDto findByKey(String userKey);

}
