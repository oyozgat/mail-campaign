package com.picus.mail.service;

import com.picus.mail.converter.MailReportConverter;
import com.picus.mail.data.MailReportDto;
import com.picus.mail.entity.MailReportEntity;
import com.picus.mail.repository.MailReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MailReportServiceImpl implements MailReportService {

    @Autowired
    private MailReportRepository repository;

    @Autowired
    private MailReportConverter converter;

    @Override
    public MailReportDto save(MailReportDto dto) {
        return converter.convertToDto(repository.save(converter.convertToEntity(dto)));
    }

    @Override
    public List<MailReportDto> findAll() {
        List<MailReportDto> mailReportDtos = new ArrayList<>();
        Iterable<MailReportEntity> iterable = repository.findAll();
        iterable.forEach(mailReportEntity -> mailReportDtos.add(converter.convertToDto(mailReportEntity)));
        return mailReportDtos;
    }

    @Override
    public MailReportDto findByKey(String userKey) {
        MailReportEntity entity = repository.findOneByKeyAndClickedTime(userKey, 0L);
        return entity != null ? converter.convertToDto(entity) : null;
    }
}
