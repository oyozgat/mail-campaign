package com.picus.mail.service;

import com.picus.mail.converter.ContactConverter;
import com.picus.mail.data.ContactDto;
import com.picus.mail.entity.ContactEntity;
import com.picus.mail.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactConverter converter;

    @Autowired
    private ContactRepository repository;

    @Override
    public ContactDto save(ContactDto dto) {
        return converter.convertToDto(repository.save(converter.convertToEntity(dto)));
    }

    @Override
    public void delete(ContactDto dto) {
        repository.deleteById(dto.getId());
    }

    @Override
    public List<ContactDto> findAll() {
        List<ContactDto> contactDtos = new ArrayList<>();
        Iterable<ContactEntity> iterable = repository.findAll();
        iterable.forEach(contactEntity -> {
            contactDtos.add(converter.convertToDto(contactEntity));
        });
        return contactDtos;
    }

    @Override
    public ContactDto findById(int id) {
        Optional<ContactEntity> optionalEntity = repository.findById(id);
        return optionalEntity.map(contactEntity -> converter.convertToDto(contactEntity)).orElse(null);
    }
}
