# mail-campaign

Server side application is developed with Spring Boot. 
MySQL is used as a database. Therefore MySQL server must be started.
Configuration properties are located in application.properties file.
There are some properties that you should change before run the application.
- upload.url property must be the path that the file will be uploaded.
- mail.link if you run the client application another ip then you should change this property
- spring.datasource.url if you want to mysql database which runs on another serve you should change this property
- spring.datasource.username and spring.datasource.password may be changed
